package com.bbyb.operating.examination.controller;

import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.po.User;
import com.bbyb.operating.examination.model.vo.CommonResult;
import com.bbyb.operating.examination.service.RoleService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author zhaodongxu
 * @date 2023/8/10 20:01
 */
@RestController
@Log4j2
public class RoleController {
    @Autowired
    private RoleService roleService;
    /**
     * 角色表查询
     * @return
     */
    @PostMapping("/roleList")
    public List<Role> roleList(){
        return roleService.roleList();
    }

    /**
     * 角色表添加
     * @param role
     * @return
     */
    @PostMapping("/roleAdd")
    public CommonResult<Boolean> roleAdd(@RequestBody Role role){
        roleService.roleAdd(role);
        return new CommonResult<>(true);
    }

    /**
     * 角色表修改
     * @param role
     * @return
     */
    @PostMapping("/roleUpd")
    public CommonResult<Boolean> roleUpd(@RequestBody Role role){
        roleService.roleUpd(role);
        return new CommonResult<>(true);
    }

    /**
     * 根据角色id删除数据
     * @param id
     * @return
     */
    @PostMapping("/roleDel/{id}")
    public CommonResult<Boolean> roleDel(@PathVariable Integer id){
        roleService.roleDel(id);
        return new CommonResult<>(true);
    }

    /**
     * 根据id查询角色表信息
     * @param id
     * @return
     */
    @PostMapping("/getRole/{id}")
    public User getRole(@PathVariable Integer id){
        return roleService.getRole(id);
    }
}
