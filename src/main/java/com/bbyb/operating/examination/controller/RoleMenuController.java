package com.bbyb.operating.examination.controller;

import com.bbyb.operating.examination.model.po.RoleMenu;
import com.bbyb.operating.examination.model.vo.CommonResult;
import com.bbyb.operating.examination.service.RoleMenuService;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author zhaodongxu
 * @date 2023/8/10 20:26
 */
@RestController
@Log4j2
public class RoleMenuController {
    @Autowired
    private RoleMenuService roleMenuService;

    @PostMapping(value = "add")
    public CommonResult<Boolean> addRoleMenu(@RequestBody RoleMenu RoleMenu){

        String errorMsg = roleMenuService.addRoleMenu(RoleMenu);
        if(StringUtils.isNotEmpty(errorMsg)){
            return new CommonResult<>(603, errorMsg);
        }
        return new CommonResult<>(true);
    }


    /**
     * 查询
     * @return
     */
    @PostMapping("/allRoleMenu")
    public List<RoleMenu> getAllRoleMenu(){
        return roleMenuService.getAllRoleMenu();
    }

    /**
     * 描述：根据ID删除用户
     * @param id
     * @return int
     * @date 2023/8/8 16:52
     */
    @PostMapping("/deleteRoleMenu/{id}")
    public CommonResult<Boolean> deleteRoleMenu(@PathVariable Integer id) {
        roleMenuService.deleteRoleMenu(id);
        return new CommonResult<>(true);
    };

    /**
     * 描述：根据ID获取用户
     * @param id
     * @return com.bbyb.operating.examination.model.po.RoleMenu
     * @date 2023/8/8 16:52
     */
    @PostMapping("/getRoleMenu/{id}")
    public RoleMenu getRoleMenu(@PathVariable Integer id) {
        return roleMenuService.getRoleMenu(id);
    };

    /**
     * 描述：更新用户
     * @param row
     * @return int
     * @date 2023/8/8 16:53
     */
    @PostMapping("/updateRoleMenu")
    public CommonResult<Boolean> updateRoleMenu(@RequestBody RoleMenu row) {
        roleMenuService.updateRoleMenu(row);
        return new CommonResult<>(true);
    };
}
