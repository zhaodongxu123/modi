package com.bbyb.operating.examination.controller;

import com.bbyb.operating.examination.model.po.RoleUser;
import com.bbyb.operating.examination.model.vo.CommonResult;
import com.bbyb.operating.examination.service.RoleUserService;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author zhaodongxu
 * @date 2023/8/10 20:10
 */
@RestController
@Log4j2
public class RoleUserController {
    @Autowired
    private RoleUserService roleUserService;
    @PostMapping(value = "add")
    public CommonResult<Boolean> addRoleUser(@RequestBody RoleUser RoleUser){
        String errorMsg = roleUserService.addRoleUser(RoleUser);
        if(StringUtils.isNotEmpty(errorMsg)){
            return new CommonResult<>(603, errorMsg);
        }
        return new CommonResult<>(true);
    }


    @PostMapping("/allRoleUser")
    public List<RoleUser> getAllRoleUser(){
        return roleUserService.getAllRoleUser();
    }

    /**
     * 描述：根据ID删除用户
     * @param id
     * @return int
     * @date 2023/8/8 16:52
     */
    @PostMapping("/deleteRoleUser/{id}")
    public CommonResult<Boolean> deleteRoleUser(@PathVariable Integer id) {
        roleUserService.deleteRoleUser(id);
        return new CommonResult<>(true);
    };

    /**
     * 描述：根据ID获取用户
     * @param id
     * @return com.bbyb.operating.examination.model.po.RoleUser
     * @date 2023/8/8 16:52
     */
    @PostMapping("/getRoleUser/{id}")
    public RoleUser getRoleUser(@PathVariable Integer id) {
        return roleUserService.getRoleUser(id);
    };

    /**
     * 描述：更新用户
     * @param row
     * @return int
     * @date 2023/8/8 16:53
     */
    @PostMapping("/updateRoleUser")
    public CommonResult<Boolean> updateRoleUser(@RequestBody RoleUser row) {
        roleUserService.updateRoleUser(row);
        return new CommonResult<>(true);
    };
}
