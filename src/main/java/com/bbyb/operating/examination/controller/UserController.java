package com.bbyb.operating.examination.controller;

import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.po.User;
import com.bbyb.operating.examination.model.vo.CommonResult;
import com.bbyb.operating.examination.service.UserService;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.xml.transform.Result;
import java.util.List;

/**
 * 账号
 * className: UserController
 * datetime: 2023/2/10 14:28
 * author: lx
 */
@RestController
@Log4j2
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * 查询登录列表
     * @return
     */
    @PostMapping("/user")
    public List<User> userList (){
        return userService.userList();
    }

    /**
     * 注册登录表
     * @param user
     * @return
     */
    @PostMapping("/userAdd")
    public CommonResult<Boolean> add(@RequestBody User user){
        userService.add(user);
        return new CommonResult<>(true);
    }

    /**
     * 根据id删除登录人
     * @param id
     * @return
     */
    @PostMapping("/userDel/{id}")
    public CommonResult<Boolean> del(@PathVariable Integer id){
        userService.del(id);
        return new CommonResult<>(true);
    }

    /**
     * 根据id查询登录人信息
     * @param id
     * @return
     */
    @PostMapping("/getUser/{id}")
    public User getUser(@PathVariable Integer id){
       return userService.user(id);
    }

    /**
     * 修改登录表
     * @param user
     * @return
     */
    @PostMapping("/userUpd")
    public CommonResult<Boolean> upd(@RequestBody User user){
        userService.upd(user);
        return new CommonResult<>(true);
    }

}



