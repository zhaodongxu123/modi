package com.bbyb.operating.examination.mapper;

import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.po.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface RoleMapper {

    List<Role> roleList();


    void roleAdd(Role role);

    void roleUpd(Role role);

    void roleDel(@Param("id") Integer id);

    User getRole(@Param("id") Integer id);
}
