package com.bbyb.operating.examination.mapper;

import com.bbyb.operating.examination.model.po.RoleMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface RoleMenuMapper {

    String addRoleMenu(RoleMenu roleMenu);

    List<RoleMenu> getAllRoleMenu();


    void deleteRoleMenu(@Param("id") Integer id);

    RoleMenu getRoleMenu(@Param("id") Integer id);

    void updateRoleMenu(RoleMenu row);

}
