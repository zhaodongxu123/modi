package com.bbyb.operating.examination.mapper;

import com.bbyb.operating.examination.model.po.RoleUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface RoleUserMapper {

    String addRoleUser(RoleUser roleUser);

    List<RoleUser> getAllRoleUser();

    void deleteRoleUser(@Param("id") Integer id);

    RoleUser getRoleUser(@Param("id") Integer id);

    void updateRoleUser(RoleUser row);
}
