package com.bbyb.operating.examination.model.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sun.istack.internal.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Max;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private Integer id;
    private String userCode;
    @Max(value = 3, message = "【每个姓名】不能超过3")
    private String userName;
    @Max(value = 11, message = "【每个手机号】不能超过11位")
    private String phone;
    @Max(value = 4, message = "【每个密码】不能超过4")
    private String password;
    @Max(value = 100, message = "【每页条数】不能超过100")
    private String email;
    private String idCard;
    private Integer gender;
    private Integer isAdmin;
    private Integer isEnable;
    private String createBy;
    private LocalDateTime createTime;
    private String updateBy;
    private LocalDateTime updateTime;
    private Integer invalid;
}
