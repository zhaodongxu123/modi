package com.bbyb.operating.examination.service;

import com.bbyb.operating.examination.model.po.RoleMenu;

import java.util.List;

public interface RoleMenuService {
    String addRoleMenu(RoleMenu roleMenu);

    List<RoleMenu> getAllRoleMenu();


    void deleteRoleMenu(Integer id);

    RoleMenu getRoleMenu(Integer id);

    void updateRoleMenu(RoleMenu row);
}
