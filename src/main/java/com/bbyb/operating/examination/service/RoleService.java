package com.bbyb.operating.examination.service;

import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.po.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleService {
    List<Role> roleList();

    void roleAdd(Role role);

    void roleUpd(Role role);

    void roleDel(@Param("id") Integer id);

    User getRole(@Param("id") Integer id);
}
