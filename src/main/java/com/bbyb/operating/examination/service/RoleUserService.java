package com.bbyb.operating.examination.service;

import com.bbyb.operating.examination.model.po.RoleUser;

import java.util.List;

public interface RoleUserService {
    String addRoleUser(RoleUser roleUser);

    List<RoleUser> getAllRoleUser();


    void deleteRoleUser(Integer id);

    RoleUser getRoleUser(Integer id);

    void updateRoleUser(RoleUser row);

}
