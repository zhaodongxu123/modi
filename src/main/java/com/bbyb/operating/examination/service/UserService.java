package com.bbyb.operating.examination.service;

import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.po.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserService {


    List<User> userList();

    void del(@Param("id") Integer id);

    void add(User user);

    User user(@Param("id") Integer id);

    void upd(User user);

}
