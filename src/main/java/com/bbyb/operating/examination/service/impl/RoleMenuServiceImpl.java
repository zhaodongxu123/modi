package com.bbyb.operating.examination.service.impl;

import com.bbyb.operating.examination.mapper.RoleMenuMapper;
import com.bbyb.operating.examination.model.po.RoleMenu;
import com.bbyb.operating.examination.service.RoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhaodongxu
 * @date 2023/8/10 20:27
 */
@Service
public class RoleMenuServiceImpl implements RoleMenuService {
    @Autowired
    private RoleMenuMapper roleMenuMapper;
    @Override
    public String addRoleMenu(RoleMenu roleMenu) {
        return roleMenuMapper.addRoleMenu(roleMenu);
    }

    @Override
    public List<RoleMenu> getAllRoleMenu() {
        return roleMenuMapper.getAllRoleMenu();
    }

    @Override
    public void deleteRoleMenu(Integer id) {
        roleMenuMapper.deleteRoleMenu(id);
    }

    @Override
    public RoleMenu getRoleMenu(Integer id) {
        return roleMenuMapper.getRoleMenu(id);
    }

    @Override
    public void updateRoleMenu(RoleMenu row) {
        roleMenuMapper.updateRoleMenu(row);
    }
}
