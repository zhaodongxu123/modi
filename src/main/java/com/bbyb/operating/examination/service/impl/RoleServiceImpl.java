package com.bbyb.operating.examination.service.impl;

import com.bbyb.operating.examination.mapper.RoleMapper;
import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.po.RoleMenu;
import com.bbyb.operating.examination.model.po.User;
import com.bbyb.operating.examination.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhaodongxu
 * @date 2023/8/10 20:03
 */
@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleMapper roleMapper;
    @Override
    public List<Role> roleList() {
        return roleMapper.roleList();
    }

    @Override
    public void roleAdd(Role role) {
        roleMapper.roleAdd(role);
    }

    @Override
    public void roleUpd(Role role) {
        roleMapper.roleUpd(role);
    }

    @Override
    public void roleDel(Integer id) {
        roleMapper.roleDel(id);
    }

    @Override
    public User getRole(Integer id) {
        return roleMapper.getRole(id);
    }
}
