package com.bbyb.operating.examination.service.impl;

import com.bbyb.operating.examination.mapper.RoleMapper;
import com.bbyb.operating.examination.mapper.RoleUserMapper;
import com.bbyb.operating.examination.model.po.RoleUser;
import com.bbyb.operating.examination.service.RoleUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhaodongxu
 * @date 2023/8/10 20:10
 */
@Service
public class RoleUserServiceImpl implements RoleUserService {
    @Autowired
    private RoleUserMapper roleMapper;
    @Override
    public String addRoleUser(RoleUser roleUser) {
        return roleMapper.addRoleUser(roleUser);
    }

    @Override
    public List<RoleUser> getAllRoleUser() {
        return roleMapper.getAllRoleUser();
    }

    @Override
    public void deleteRoleUser(Integer id) {
        roleMapper.deleteRoleUser(id);
    }

    @Override
    public RoleUser getRoleUser(Integer id) {
        return roleMapper.getRoleUser(id);
    }

    @Override
    public void updateRoleUser(RoleUser row) {
        roleMapper.updateRoleUser(row);
    }
}
