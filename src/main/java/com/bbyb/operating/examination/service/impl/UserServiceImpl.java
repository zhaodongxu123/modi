package com.bbyb.operating.examination.service.impl;

import com.bbyb.operating.examination.mapper.UserMapper;
import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.po.User;
import com.bbyb.operating.examination.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 账号服务
 * className: UserServiceImpl
 * datetime: 2023/2/10 14:29
 * author: lx
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> userList() {
        return userMapper.userList();
    }

    @Override
    public void del(Integer id) {
        userMapper.del(id);
    }

    @Override
    public void add(User user) {
        userMapper.add(user);
    }

    @Override
    public User user(Integer id) {
        return userMapper.user(id);
    }

    @Override
    public void upd(User user) {
        userMapper.upd(user);
    }
}
